namespace :table_seeder do

  desc "Fills the tables with random data."
  task fill_data: :environment do
    puts "Table filling STARTED #{Time.now}"
    fill_tbl
    puts "Table filling ENDED #{Time.now}"
  end

  def fill_tbl

    service_provider = ['vodafone', 'airtel', 'idea', 'tata', 'jio', 'bsnl']

    (1..100).each do |index|
      cmpnant_pd = random_dist
      cmpnant_ps = random_ps(cmpnant_pd)

      nant = Complainant.new(
        name: Faker::Name.name,
        address: random_place,
        ps: cmpnant_ps,
        ps_dist: cmpnant_pd,
        state: state_random,
        contact_no: Faker::Number.number(digits: 10)
      )

      nant.complaints.build(
        comp_recvd_on: random_date,
        ref_gde_no: "#{random_ps(random_dist)}/#{random_date}",
        gde_dt: random_date,
        gde_ps: random_ps(random_dist),
        fir_no: "#{random_ps(random_dist)}/#{random_date}",
        fir_dt: random_date,
        fir_ps: random_ps(random_dist),
        usec: Faker::Number.within(range: 1..34)
      )

      occr_pd = random_dist
      occr_ps = random_ps(occr_pd)

      nant.occurances.build(
        date: random_date,
        time: random_time,
        place: random_place,
        ps_dist: occr_pd,
        ps: occr_ps,
        state: state_random,
        description: Faker::Lorem.paragraph
      )

      nant.stolen_mobiles.build(
        imei: Faker::Code.imei,
        stolen_mobno: Faker::Number.number(digits: 10),
        service_provider: service_provider[Faker::Number.between(from: 1, to: 6)],
        make: Faker::Device.manufacturer,
        model: Faker::Device.model_name,
        color: Faker::Color.color_name
      )

      vic_pd = random_dist
      vic_ps = random_ps(vic_pd)

      nant.victims.build(
        vic_name: Faker::Name.name,
        vic_addr: random_place,
        ps: vic_ps,
        ps_dist: vic_pd,
        state: state_random,
        vic_contct: Faker::Number.number(digits: 10)
      )

      begin
        # debugger
        nant.save
        puts "-----#{index}&&&&&&&&&&&&&&&&&&&&&& #{nant.to_json}"
      rescue Exception => e
        puts "EEEEEEEEEEEEEEEEXXXXXXXXXXXCCCCCCCCCCCCEEEEEEEEEEPPPPPP #{e}"
      end
    end
  end

  private

  def random_ps
    Faker::Number.within(range: 1..1000)
  end

  def random_date
    Faker::Date.between(from: 1.year.ago, to: Date.today)
  end

  def random_time
    Faker::Time.between(from: Date.today, to: Date.today + 1).strftime('%H:%M:%S')
  end

  def random_place
    Faker::Address.street_address
  end

  def random_dist
    # Faker::Number.within(range: 1..40)
    ps_dist_arr = PoliceDistrict.pluck(:ps_dist_code).uniq
    puts "PPPPPPPPPPPPPPPPPDDDDDDDDDDDDDDDDD #{ps_dist_arr}"
    ps_dist_arr[Faker::Number.within(range: 0...ps_dist_arr.length)]
  end

  def random_ps(pd)
    puts "PD========== #{pd}"
    ps_arr = PoliceStation.where(ps_dist_code: pd).pluck(:ps_code)
    puts "PPPPPPPPPPPPPPPSSSSSSSSSSSSSSSsSSS #{ps_arr}"
    ps_arr[Faker::Number.within(range: 0...ps_arr.length)]
  end

  def state_random
    # Faker::Number::digit
    Faker::Number.within(range: 1..36)
  end
end
