Rails.application.routes.draw do
  get 'find/search'
  get 'home/get_ps'
  devise_for :users
  root to: "home#index"
  #root to: 'complainants#index'

  resources :complaints
  resources :occurances
  resources :victims
  resources :stolen_mobiles
  resources :complainants
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
