$(document).ready(function() {

    pol_stns = $("#complainant_ps").html();
    // console.log("police_stations = " + pol_stns);

    $("#complainant_ps_dist").change(function() {
        ps_dist_selected = $("#complainant_ps_dist :selected").text();
        pol_stns_selected = $(pol_stns).filter("optgroup[label='" + ps_dist_selected + "']").html();

        if (pol_stns_selected) {
            // console.log("police stations selected = " + pol_stns_selected);
            $("#complainant_ps").html(pol_stns_selected);
        } else {
            console.log("blanks");
            $("#complainant_ps").hide();
        }
    });

    $("#complainant_occurances_attributes_0_ps_dist").change(function() {
        ps_dist_selected = $("#complainant_occurances_attributes_0_ps_dist :selected").text();
        pol_stns_selected = $(pol_stns).filter("optgroup[label='" + ps_dist_selected + "']").html();

        if (pol_stns_selected) {
            // console.log("police stations selected = " + pol_stns_selected);
            $("#complainant_occurances_attributes_0_ps").html(pol_stns_selected);
        } else {
            console.log("blanks");
            $("#complainant_occurances_attributes_0_ps").hide();
        }
    });

    // $("#complainant_ps_dist").change(function() {
    //     // alert($("#complainant_ps_dist option:selected").text() + " " + $("#complainant_ps_dist option:selected").val());

    //     $.ajax({
    //         url: "/home/get_ps",
    //         data: {
    //             ps_dist_id: $("#complainant_ps_dist option:selected").val()
    //         },
    //         success: function(result) {
    //             // console.log(result);
    //             str = "<option value=''>Select Police Station</option>";

    //             $.each(result, function(key, val) {
    //                 // console.log("key = " + key + " | " + "value = " + val["ps_name"]);
    //                 str += "<option value = '" + val["ps_code"] + "'>" + val["ps_name"] + "</option>";
    //             });
    //             $("#complainant_ps").html(str);
    //         },
    //     });
    //     // console.log("AJAX CALLED");
    // });

    // $("#complainant_occurances_attributes_0_ps_dist").change(function() {
    //     $.ajax({
    //         url: "/home/get_ps",
    //         data: {
    //             ps_dist_id: $("#complainant_occurances_attributes_0_ps_dist option:selected").val()
    //         },
    //         success: function(result) {
    //             // console.log(result);
    //             str = "<option value=''>Select Police Station</option>";

    //             $.each(result, function(key, val) {
    //                 // console.log("key = " + key + " | " + "value = " + val["ps_name"]);
    //                 str += "<option value = '" + val["ps_code"] + "'>" + val["ps_name"] + "</option>";
    //             });
    //             $("#complainant_occurances_attributes_0_ps").html(str);
    //         },
    //     });
    //     // console.log("AJAX CALLED");
    // });

    // $("#complainant_victims_attributes_0_ps_dist").change(function() {
    //     // alert($("#complainant_victims_attributes_0_ps_dist option:selected").text() + " " + $("#complainant_victims_attributes_0_ps_dist option:selected").val());

    //     $.ajax({
    //         url: "/home/get_ps",
    //         data: {
    //             ps_dist_id:  $("#complainant_victims_attributes_0_ps_dist option:selected").val()
    //         },
    //         success: function(result){
    //             // console.log(result);
    //             str = "<option value=''>Select Police Station</option>";

    //             $.each(result, function(key, val){
    //                 // console.log("key = " + key + " | " + "value = " + val["ps_name"]);
    //                 str += "<option value = '" + val["ps_code"] + "'>" + val["ps_name"] + "</option>";
    //             });
    //             $("#complainant_victims_attributes_0_ps").html(str);
    //         },
    //     });
    //     // console.log("AJAX CALLED");
    // });

});
