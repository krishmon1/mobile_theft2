class PoliceDistrict < ApplicationRecord
  has_many :police_stations, foreign_key: 'ps_dist_code', primary_key: 'ps_dist_code'
  validates_uniqueness_of :ps_dist_code
end
