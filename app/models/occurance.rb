class Occurance < ApplicationRecord
  belongs_to :complainant

  has_rich_text :desc_content
end
