class Complainant < ApplicationRecord

  has_many :complaints, dependent: :destroy, inverse_of: :complainant
	accepts_nested_attributes_for :complaints, allow_destroy: true, reject_if: :all_blank

  has_many :occurances, dependent: :destroy, inverse_of: :complainant
  accepts_nested_attributes_for :occurances, allow_destroy: true, reject_if: :all_blank

  has_many :stolen_mobiles, dependent: :destroy, inverse_of: :complainant
  accepts_nested_attributes_for :stolen_mobiles, allow_destroy: true, reject_if: :all_blank

  has_many :victims, dependent: :destroy, inverse_of: :complainant
  accepts_nested_attributes_for :victims, allow_destroy: true, reject_if: :all_blank

  # belongs_to :police_district
  # belongs_to :police_station
end
