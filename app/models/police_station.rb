class PoliceStation < ApplicationRecord
    belongs_to :police_district, foreign_key: 'ps_dist_code', primary_key: 'ps_dist_code'
    validates_uniqueness_of :ps_code
end
