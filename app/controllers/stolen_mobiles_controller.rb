class StolenMobilesController < ApplicationController
  before_action :set_stolen_mobile, only: [:show, :edit, :update, :destroy]

  # GET /stolen_mobiles
  # GET /stolen_mobiles.json
  def index
    @stolen_mobiles = StolenMobile.all
  end

  # GET /stolen_mobiles/1
  # GET /stolen_mobiles/1.json
  def show
  end

  # GET /stolen_mobiles/new
  def new
    @stolen_mobile = StolenMobile.new
  end

  # GET /stolen_mobiles/1/edit
  def edit
  end

  # POST /stolen_mobiles
  # POST /stolen_mobiles.json
  def create
    @stolen_mobile = StolenMobile.new(stolen_mobile_params)

    respond_to do |format|
      if @stolen_mobile.save
        format.html { redirect_to @stolen_mobile, notice: 'Stolen mobile was successfully created.' }
        format.json { render :show, status: :created, location: @stolen_mobile }
      else
        format.html { render :new }
        format.json { render json: @stolen_mobile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stolen_mobiles/1
  # PATCH/PUT /stolen_mobiles/1.json
  def update
    respond_to do |format|
      if @stolen_mobile.update(stolen_mobile_params)
        format.html { redirect_to @stolen_mobile, notice: 'Stolen mobile was successfully updated.' }
        format.json { render :show, status: :ok, location: @stolen_mobile }
      else
        format.html { render :edit }
        format.json { render json: @stolen_mobile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stolen_mobiles/1
  # DELETE /stolen_mobiles/1.json
  def destroy
    @stolen_mobile.destroy
    respond_to do |format|
      format.html { redirect_to stolen_mobiles_url, notice: 'Stolen mobile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stolen_mobile
      @stolen_mobile = StolenMobile.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def stolen_mobile_params
      params.require(:stolen_mobile).permit(:imei, :stolen_mobno, :service_provider, :make, :model, :color)
    end
end
