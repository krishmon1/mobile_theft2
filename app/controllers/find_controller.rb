class FindController < ApplicationController
  def search
    # p "PARAMS::::::::::::::::::::::::::::::::::::: #{params}"

    if params[:query].blank?
      redirect_to(root_path, alert: "Empty field!") and return
    else
      # @results = Complainant.where(contact_no: params[:query])
      @results = Complainant.where("lower(name) like ? or lower(address) like ? or contact_no like ?", "%#{params[:query].downcase}%", "%#{params[:query].downcase}%", "%#{params[:query].downcase}%")
    end
  end
end
