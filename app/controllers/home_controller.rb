class HomeController < ApplicationController
  def index
    @complainants = Complainant.all
    @complaints = Complaint.all
    @victims = Victim.all
    @stolen_mobiles = StolenMobile.all
    @occurances = Occurance.all
  end

  def get_ps
    render json: PoliceStation.select(:ps_code, :ps_name).where(ps_dist_code: params[:ps_dist_id])
  end
end
