class ComplainantsController < ApplicationController
  before_action :set_complainant, only: [:show, :edit, :update, :destroy]

  # GET /complainants
  # GET /complainants.json
  def index
    @complainants = Complainant.all.paginate(page: params[:page], per_page: 10)
  end

  # GET /complainants/1
  # GET /complainants/1.json
  def show
  end

  # GET /complainants/new
  def new
    @complainant = Complainant.new
    @complainant.complaints.build
    @complainant.occurances.build
    @complainant.stolen_mobiles.build
    @complainant.victims.build

    # puts "NNNNNNNNNNNNNNEWWWWWWWWWWWWWWWW"
    # @pol_dists = PoliceDistrict.select("ps_dist_code as id", :police_district_name).order("police_district_name")
    # puts "#{@pol_dists.to_json}NNNNNNNNNNNNNEEEEEEEEEEWWWWWW EEEnd"
  end

  # GET /complainants/1/edit
  def edit
  end

  # POST /complainants
  # POST /complainants.json
  def create
    @complainant = Complainant.new(complainant_params)

    respond_to do |format|
      if @complainant.save
        format.html { redirect_to @complainant, notice: 'Complainant was successfully created.' }
        format.json { render :show, status: :created, location: @complainant }
      else
        format.html { render :new }
        format.json { render json: @complainant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /complainants/1
  # PATCH/PUT /complainants/1.json
  def update
    respond_to do |format|
      if @complainant.update(complainant_params)
        format.html { redirect_to @complainant, notice: 'Complainant was successfully updated.' }
        format.json { render :show, status: :ok, location: @complainant }
      else
        format.html { render :edit }
        format.json { render json: @complainant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /complainants/1
  # DELETE /complainants/1.json
  def destroy
    @complainant.destroy
    respond_to do |format|
      format.html { redirect_to complainants_url, notice: 'Complainant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_complainant
      @complainant = Complainant.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def complainant_params
      params.require(:complainant)
        .permit(:name, :address, :ps, :ps_dist, :state, :contact_no,
                complaints_attributes: [:id, :comp_recvd_on, :ref_gde_no, :gde_dt, :gde_ps, :fir_no, :fir_dt, :fir_ps, :usec],
                occurances_attributes: [:id, :date, :time, :place, :ps_dist, :ps, :state, :desc_content],
                stolen_mobiles_attributes: [:id, :imei, :stolen_mobno, :service_provider, :make, :model, :color],
                victims_attributes: [:id, :vic_name, :vic_addr, :ps, :ps_dist, :state, :vic_contct]
                )
    end
end
