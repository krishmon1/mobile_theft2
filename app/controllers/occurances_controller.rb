class OccurancesController < ApplicationController
  before_action :set_occurance, only: [:show, :edit, :update, :destroy]

  # GET /occurances
  # GET /occurances.json
  def index
    @occurances = Occurance.all
  end

  # GET /occurances/1
  # GET /occurances/1.json
  def show
  end

  # GET /occurances/new
  def new
    @occurance = Occurance.new
  end

  # GET /occurances/1/edit
  def edit
  end

  # POST /occurances
  # POST /occurances.json
  def create
    @occurance = Occurance.new(occurance_params)

    respond_to do |format|
      if @occurance.save
        format.html { redirect_to @occurance, notice: 'Occurance was successfully created.' }
        format.json { render :show, status: :created, location: @occurance }
      else
        format.html { render :new }
        format.json { render json: @occurance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /occurances/1
  # PATCH/PUT /occurances/1.json
  def update
    respond_to do |format|
      if @occurance.update(occurance_params)
        format.html { redirect_to @occurance, notice: 'Occurance was successfully updated.' }
        format.json { render :show, status: :ok, location: @occurance }
      else
        format.html { render :edit }
        format.json { render json: @occurance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /occurances/1
  # DELETE /occurances/1.json
  def destroy
    @occurance.destroy
    respond_to do |format|
      format.html { redirect_to occurances_url, notice: 'Occurance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_occurance
      @occurance = Occurance.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def occurance_params
      params.require(:occurance).permit(:date, :time, :place, :ps_dist, :ps, :state, :description)
    end
end
