json.extract! complaint, :id, :comp_recvd_on, :ref_gde_no, :gde_dt, :gde_ps, :fir_no, :fir_dt, :fir_ps, :usec, :created_at, :updated_at
json.url complaint_url(complaint, format: :json)
