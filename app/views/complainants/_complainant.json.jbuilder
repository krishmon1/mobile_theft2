json.extract! complainant, :id, :name, :address, :ps, :ps_dist, :state, :contact_no, :created_at, :updated_at
json.url complainant_url(complainant, format: :json)
