json.extract! occurance, :id, :date, :time, :place, :ps_dist, :ps, :state, :description, :created_at, :updated_at
json.url occurance_url(occurance, format: :json)
