json.extract! stolen_mobile, :id, :imei, :stolen_mobno, :service_provider, :make, :model, :color, :created_at, :updated_at
json.url stolen_mobile_url(stolen_mobile, format: :json)
