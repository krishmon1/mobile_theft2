class CreateComplaints < ActiveRecord::Migration[6.0]
  def change
    create_table :complaints do |t|
      t.date :comp_recvd_on
      t.string :ref_gde_no
      t.date :gde_dt
      t.string :gde_ps
      t.string :fir_no
      t.date :fir_dt
      t.string :fir_ps
      t.string :usec

      t.timestamps
    end
  end
end
