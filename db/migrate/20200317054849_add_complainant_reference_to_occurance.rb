class AddComplainantReferenceToOccurance < ActiveRecord::Migration[6.0]
  def change
    add_reference :occurances, :complainant, null: false, foreign_key: true
  end
end
