class AddComplainantReferenceToStolenMobile < ActiveRecord::Migration[6.0]
  def change
    add_reference :stolen_mobiles, :complainant, null: false, foreign_key: true
  end
end
