class PoliceStations < ActiveRecord::Migration[6.0]
  def self.up
    create_table :police_stations do |t|
      t.string :ps_code
      t.string :ps_name
      t.string :ps_dist_code

      t.timestamps
    end
    # add_foreign_key :police_stations, :police_districts, column: :ps_dist_code, primary_key: :ps_dist_code
    # add_reference :police_stations, :ps_dist
  end


  def self.down

  end
end
