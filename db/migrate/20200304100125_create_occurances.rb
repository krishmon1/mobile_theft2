class CreateOccurances < ActiveRecord::Migration[6.0]
  def change
    create_table :occurances do |t|
      t.date :date
      t.time :time
      t.string :place
      t.string :ps_dist
      t.string :ps
      t.integer :state
      t.text :description

      t.timestamps
    end
  end
end
