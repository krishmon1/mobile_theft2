class CreateVictims < ActiveRecord::Migration[6.0]
  def change
    create_table :victims do |t|
      t.string :vic_name
      t.string :vic_addr
      t.string :ps
      t.string :ps_dist
      t.integer :state
      t.string :vic_contct

      t.timestamps
    end
  end
end
