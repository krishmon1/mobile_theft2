class CreateComplainants < ActiveRecord::Migration[6.0]
  def change
    create_table :complainants do |t|
      t.string :name
      t.string :address
      t.string :ps
      t.string :ps_dist
      t.string :state
      t.string :contact_no

      t.timestamps
    end
  end
end
