class States < ActiveRecord::Migration[6.0]
  def self.up
    create_table :states do |t|
      t.integer :state_code
      t.string :state_name

      t.timestamps
    end
  end

   
  def self.down
    
  end
end
