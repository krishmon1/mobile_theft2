class CreateStolenMobiles < ActiveRecord::Migration[6.0]
  def change
    create_table :stolen_mobiles do |t|
      t.string :imei
      t.string :stolen_mobno
      t.string :service_provider
      t.string :make
      t.string :model
      t.string :color

      t.timestamps
    end
  end
end
