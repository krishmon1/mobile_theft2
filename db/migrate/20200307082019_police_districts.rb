class PoliceDistricts < ActiveRecord::Migration[6.0]
  def self.up
    create_table :police_districts do |t|
      t.string :ps_dist_code
      t.string :ps_dist_name

      t.timestamps
    end

    # add_index :police_districts, :ps_dist_code, unique: true
  end

   
  def self.down
    
  end

end
