# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(
  email: "admin@test.te",
  password: "admin1234",
  password_confirmation: "admin1234"
)
puts "Admin Created."

puts "Police District seeding start."

csv_text = File.read(Rails.root.join('lib', 'seeds', 'Districtd.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  pd = PoliceDistrict.new
  pd.ps_dist_code = row['ps_dist_code']
  pd.ps_dist_name = row['ps_dist_name']
  pd.save
#   puts "#{pd.ps_dist_code}, #{pd.ps_dist_name} saved"
end

puts "Police District seeding done. COUNT: #{PoliceDistrict.count}"


puts "Police Station seeding start."

csv_text = File.read(Rails.root.join('lib', 'seeds', 'PSs_221118.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  ps = PoliceStation.new
  ps.ps_code = row['ps_code']
  ps.ps_name = row['ps_name']
  ps.ps_dist_code = row['ps_dist_code']
  ps.save
#   puts "#{ps.ps_code}, #{ps.ps_name} saved"
end

puts "Police Station seeding done. COUNT: #{PoliceStation.count}"
