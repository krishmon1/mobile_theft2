# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_24_084526) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "complainants", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "ps"
    t.string "ps_dist"
    t.string "state"
    t.string "contact_no"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "complaints", force: :cascade do |t|
    t.date "comp_recvd_on"
    t.string "ref_gde_no"
    t.date "gde_dt"
    t.string "gde_ps"
    t.string "fir_no"
    t.date "fir_dt"
    t.string "fir_ps"
    t.string "usec"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "complainant_id", null: false
    t.index ["complainant_id"], name: "index_complaints_on_complainant_id"
  end

  create_table "occurances", force: :cascade do |t|
    t.date "date"
    t.time "time"
    t.string "place"
    t.string "ps_dist"
    t.string "ps"
    t.integer "state"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "complainant_id", null: false
    t.index ["complainant_id"], name: "index_occurances_on_complainant_id"
  end

  create_table "police_districts", force: :cascade do |t|
    t.string "ps_dist_code"
    t.string "ps_dist_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "police_stations", force: :cascade do |t|
    t.string "ps_code"
    t.string "ps_name"
    t.string "ps_dist_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "states", force: :cascade do |t|
    t.integer "state_code"
    t.string "state_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "stolen_mobiles", force: :cascade do |t|
    t.string "imei"
    t.string "stolen_mobno"
    t.string "service_provider"
    t.string "make"
    t.string "model"
    t.string "color"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "complainant_id", null: false
    t.index ["complainant_id"], name: "index_stolen_mobiles_on_complainant_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "victims", force: :cascade do |t|
    t.string "vic_name"
    t.string "vic_addr"
    t.string "ps"
    t.string "ps_dist"
    t.integer "state"
    t.string "vic_contct"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "complainant_id", null: false
    t.index ["complainant_id"], name: "index_victims_on_complainant_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "complaints", "complainants"
  add_foreign_key "occurances", "complainants"
  add_foreign_key "stolen_mobiles", "complainants"
  add_foreign_key "victims", "complainants"
end
